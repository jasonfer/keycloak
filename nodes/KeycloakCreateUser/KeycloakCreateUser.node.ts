import { INodeType, INodeTypeDescription, IExecuteFunctions } from 'n8n-workflow';
import { KeycloakUser } from '../../credentials/KeycloakUser';

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakCreateUser implements INodeType {
	description: INodeTypeDescription = {
		displayName: 'Create Keycloak User',
		name: 'keycloakCreateUser',
		icon: 'file:KeycloakCreateUser.node.svg', // Provide a path to your icon
		group: ['input'],
		version: 2,
		description: 'Create User in Keycloak',
		defaults: {
			name: 'Create Keycloak User',
		},
		inputs: ['main'],
		// eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
		outputs: ['main'],
		outputNames: [],
		credentials: [
			{
				name: 'keycloakCredentialsApi',
				required: true,
			},
		],
		properties: [
			{
				displayName: 'User Name',
				name: 'username',
				type: 'string',
				placeholder: 'jdoe@example.com',
				default: '',
				description: 'The user  to create in Keycloak',
			},
			{
				displayName: 'First Name',
				name: 'firstName',
				type: 'string',
				placeholder: 'John',
				default: '',
				description: 'User First Name',
			},
			{
				displayName: 'Last Name',
				name: 'lastName',
				type: 'string',
				placeholder: 'Doe',
				default: '',
				description: 'User Last Name',
			},
			{
				displayName: 'Email',
				name: 'email',
				type: 'string',
				placeholder: 'jdoe@example.com',
				default: '',
				description: 'User Email',
			},
			{
				displayName: 'Enabled',
				name: 'enabled',
				type: 'boolean',
				default: true,
				description: 'User Enabled',
			},
			{
				displayName: 'Password',
				name: 'Password',
				type: 'string',
				placeholder: '***',
				default: '',
				description: 'User password',
			},
			{
				displayName: 'Groups',
				name: 'groups',
				type: 'string',
				default: '',
				description: 'Comma separated list of groups',
			},
			{
				displayName: 'Organization Id',
				name: 'OrgID',
				type: 'string',
				default: '',
				description: 'The Organization Id for the user in myEZcare App',
			},
			{
				displayName: 'Temporary',
				name: 'temporary',
				type: 'boolean',
				default: false,
				description: 'Temporary password?',
			},
			{
				displayName: 'Realm',
				name: 'realm',
				type: 'string',
				default: '',
				description: 'The Keycloak realm under which the email check should be performed',
			},
			{
				displayName: 'Grant Type',
				name: 'grantType',
				type: 'options',
				options: [
					{
						name: 'Client Credentials',
						value: 'client_credentials',
					},
					// Add other grant types as needed
				],
				default: 'client_credentials',
				description: 'The OAuth 2.0 grant type for obtaining the token',
			},
			{
				displayName: 'Active Token',
				name: 'activeToken',
				type: 'string',

				default: '',
				description: 'Token for auth Keycloak',
			},
		],
	};

	async execute(this: IExecuteFunctions): Promise<any> {
		let outputData: any = [];

		const items = this.getInputData();
		const realm: string = this.getNodeParameter('realm', 0) as string;
		const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
		const grantType = this.getNodeParameter('grantType', 0) as string;

		const credentials = (await this.getCredentials('keycloakCredentialsApi')) as {
			serverUrl: string;
			apiToken: string;
			clientId?: string;
			clientSecret?: string;
			defaultRealm?: string;
		};
		let token: any = activeToken || '';
		if (!token && credentials.apiToken) token = credentials.apiToken;

		const userEndpoint = `${credentials.serverUrl}realms/${
			realm || credentials.defaultRealm
		}/protocol/openid-connect/token`;
		if (!token && credentials.clientId && credentials.clientSecret) {
			//get token
			const requestObj = {
				method: 'POST',
				uri: userEndpoint,
				body: `grant_type=${grantType}&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
				json: true,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					Authorization:
						'Basic ' +
						Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64'),
				},
			};

			const responseData = await this.helpers.request(requestObj);

			token = responseData.access_token;
		}
		if (!token) {
			outputData.push({
				json: { result: false, message: 'Failed to obtain Keycloak token.' },
			});
			return outputData;
		}

		const output = [];

		try {
			for (let i = 0; i < items.length; i++) {
				const item = items[i];
				console.log('Groups 🚀🚀🚀 ~ line 179', this.getNodeParameter('groups', i) as string);
				const newUser: KeycloakUser = {
					firstName: this.getNodeParameter('firstName', i) as string,
					lastName: this.getNodeParameter('lastName', i) as string,
					username: this.getNodeParameter('username', i) as string,
					email: this.getNodeParameter('email', i) as string,
					enabled: this.getNodeParameter('enabled', i) as string,
					credentials: [
						{
							type: 'password',
							value: this.getNodeParameter('Password', 0) as string,
							temporary: this.getNodeParameter('temporary', 0) as string,
						},
					],
					groups: [this.getNodeParameter('groups', i) as string],
					attributes: {
						OrgId: [this.getNodeParameter('OrgID', i) as string],
					},
				};

				//const encodedUserName = encodeURIComponent(`${newUser}`);
				const apiUrl = `${credentials.serverUrl}admin/realms/${
					realm || credentials.defaultRealm
				}/users`;
				const headers = {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				};
				const requestObj = {
					method: 'POST',
					uri: apiUrl,
					headers: headers,
					body: newUser, // Directly pass the object here
					json: true, // This tells the library to send the body as JSON
				};
				console.log('User Object 🚀🚀🚀 ~ line 189', apiUrl);
				console.log('User Object 🚀🚀🚀 ~ line 191', JSON.stringify(`${newUser}`));
				console.log('User Object 🚀🚀🚀 ~ line 194', newUser);

				const responseData = await this.helpers.request(requestObj);
				console.log('Response Data 🔴🔴🔴🔴', responseData);
				if (responseData === undefined) {
					item.json = { result: 200, message: 'User Created' };
					//return [[{ json: { result: true, message: 'User Created' } }]];
				} else {
					item.json = { result: 400, message: 'Failed to create user', data: responseData };
					//return [this.helpers.returnJsonArray({ responseData })];
				}
				output.push(item);
			}
		} catch (error) {
			return [
				[
					{
						json: {
							result: false,
							message: error.message,
							errorstack: error.stack,
						},
					},
				],
			];
		}
		return [this.helpers.returnJsonArray(output)];
	}
}
