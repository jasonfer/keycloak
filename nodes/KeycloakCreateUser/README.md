# Keycloak Create User Node

This node provides functionality to create a new user within a specified realm in Keycloak.

![Keycloak Create User Icon](KeycloakCreateUser.node.svg)

## Description

The `Keycloak Create User` node allows you to programmatically create a new user in a Keycloak realm. It can set various user attributes and return the result of the user creation process.

## Configuration

### Inputs

- **Main Input**: This is where you provide the data for the user to be created.

### Outputs

- **success**: Indicates that the user was successfully created in the specified Keycloak realm.
- **error**: Indicates an error in the user creation process, such as authentication issues, server errors, or validation failures.

### Credentials

- **keycloakCredentialsApi**: These credentials are required for this node. They should include the Server URL, optionally an API Token, Client ID, Client Secret, and a default realm.

### Parameters

- **Username**: The username for the new user. This is required.

- **Email**: The email address for the new user. This is required.

- **First Name**: The first name of the new user.

- **Last Name**: The last name of the new user.

- **Password**: The initial password for the new user. If not provided, Keycloak will require the user to set a password on first login.

- **Enabled**: Whether the new user account should be enabled or disabled.

- **Email Verified**: Whether the email address of the new user should be marked as verified.

- **Realm**: The Keycloak realm in which the user should be created. If this is left blank, the default realm provided in the credentials will be used.

- **Grant Type**: The OAuth 2.0 grant type used for obtaining the token. Currently, the only available option is `Client Credentials`, but more can be added if necessary.

- **Active Token**: A token for authenticating with Keycloak. If this is left blank and if the API Token is also not provided in the credentials, the node will try to obtain a token using the Client ID and Client Secret.

## Usage

1. Connect your data source containing user information to the main input.
2. Provide the necessary parameters such as username, email, and other user details.
3. Provide the necessary Keycloak credentials or an Active Token.
4. Execute the node. You will receive either a `success` or `error` output depending on the result of the user creation process.

## Example

Assuming you want to create a new user "john.doe" in the Keycloak realm "myRealm":

1. Connect your data source to the main input.
2. Set the `Username` parameter to "john.doe".
3. Set the `Email` parameter to "john.doe@example.com".
4. Set the `First Name` parameter to "John".
5. Set the `Last Name` parameter to "Doe".
6. Set the `Password` parameter if you want to set an initial password.
7. Set the `Enabled` parameter to true.
8. Set the `Email Verified` parameter as needed.
9. Set the `Realm` parameter to "myRealm".
10. Choose `Client Credentials` as the Grant Type.
11. Provide the necessary Keycloak credentials or an Active Token.
12. Execute the node. If the user is created successfully, you will get a `success` output. In case of any errors, the `error` output will be triggered.

Always ensure sensitive data, like tokens, credentials, and user information, are handled securely.
