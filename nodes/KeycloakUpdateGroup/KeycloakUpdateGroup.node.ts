import { INodeType, INodeTypeDescription, IExecuteFunctions } from 'n8n-workflow';

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakUpdateGroup implements INodeType {
	description: INodeTypeDescription = {
		displayName: 'Update Keycloak Group',
		name: 'keycloakUpdateGroup',
		icon: 'file:keycloakUpdateGroup.node.svg', // Provide a path to your icon
		group: ['input'],
		version: 2,
		description: 'Update Group in Keycloak',
		defaults: {
			name: 'Update Keycloak Group',
		},
		inputs: ['main'],
		// eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
		outputs: ['main'],
		outputNames: [],
		credentials: [
			{
				name: 'keycloakCredentialsApi',
				required: true,
			},
		],
		properties: [
			{
				displayName: 'Group Id',
				name: 'groupId',
				type: 'string',
				placeholder: '',
				default: '',
				description: 'The group id(guid) to Update in Keycloak',
			},
			{
				displayName: 'User Id',
				name: 'userId',
				type: 'string',
				placeholder: '',
				default: '',
				description: 'The user id(guid) to keycloak group',
			},
			{
				displayName: 'Realm',
				name: 'realm',
				type: 'string',
				default: '',
				description: 'The Keycloak realm under which the email check should be performed',
			},
			{
				displayName: 'Grant Type',
				name: 'grantType',
				type: 'options',
				options: [
					{
						name: 'Client Credentials',
						value: 'client_credentials',
					},
					// Add other grant types as needed
				],
				default: 'client_credentials',
				description: 'The OAuth 2.0 grant type for obtaining the token',
			},
			{
				displayName: 'Active Token',
				name: 'activeToken',
				type: 'string',

				default: '',
				description: 'Token for auth Keycloak',
			},
		],
	};

	async execute(this: IExecuteFunctions): Promise<any> {
		let outputData: any = [];

		//const items = this.getInputData();
		const realm: string = this.getNodeParameter('realm', 0) as string;
		const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
		const grantType = this.getNodeParameter('grantType', 0) as string;

		const credentials = (await this.getCredentials('keycloakCredentialsApi')) as {
			serverUrl: string;
			apiToken: string;
			clientId?: string;
			clientSecret?: string;
			defaultRealm?: string;
		};
		let token: any = activeToken || '';
		if (!token && credentials.apiToken) token = credentials.apiToken;

		const userEndpoint = `${credentials.serverUrl}realms/${
			realm || credentials.defaultRealm
		}/protocol/openid-connect/token`;
		if (!token && credentials.clientId && credentials.clientSecret) {
			//get token
			const requestObj = {
				method: 'POST',
				uri: userEndpoint,
				body: `grant_type=${grantType}&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
				json: true,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					Authorization:
						'Basic ' +
						Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64'),
				},
			};

			const responseData = await this.helpers.request(requestObj);

			token = responseData.access_token;
		}
		if (!token) {
			outputData.push({
				json: { result: false, message: 'Failed to obtain Keycloak token.' },
			});
			return outputData;
		}

		try {
			//for (let i = 0; i < items.length; i++) {
			//const item = items[0];
			const groupId = this.getNodeParameter('groupId', 0) as string;
			const userId = this.getNodeParameter('userId', 0) as string;

			const apiUrl = `${credentials.serverUrl}admin/realms/${
				realm || credentials.defaultRealm
			}/users/${userId}/groups/${groupId}`;
			const headers = {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			};
			const requestObj = {
				method: 'PUT',
				uri: apiUrl,
				headers: headers,
				body: JSON.stringify({
					realm: `${realm}`,
					'user-id': `${userId}`,
					groupId: `${groupId}`,
				}),
			};

			const responseData = await this.helpers.request(requestObj);
			console.log('Response Data', responseData);
			if (responseData == undefined) {
				return [[{ json: { result: true, message: 'Group Updated' } }]];
			} else {
				return [this.helpers.returnJsonArray({ responseData })];
			}
		} catch (error) {
			return [
				[
					{
						json: {
							result: false,
							message: error.message,
							errorstack: error.stack,
						},
					},
				],
			];
		}
	}
}
