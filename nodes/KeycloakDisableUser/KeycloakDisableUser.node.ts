import { INodeType, INodeTypeDescription, IExecuteFunctions } from 'n8n-workflow';
/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakDisableUser implements INodeType {
	description: INodeTypeDescription = {
		displayName: 'Disable Keycloak User',
		name: 'keycloakDisableUser',
		icon: 'file:keycloakDisableUser.node.svg', // Provide a path to your icon
		group: ['input'],
		version: 2,
		description: 'Disable User in Keycloak',
		defaults: {
			name: 'Disable Keycloak User',
		},
		inputs: ['main'],
		// eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
		outputs: ['main'],
		outputNames: [],
		credentials: [
			{
				name: 'keycloakCredentialsApi',
				required: true,
			},
		],
		properties: [
			{
				displayName: 'User Name',
				name: 'username',
				type: 'string',
				placeholder: 'jdoe@example.com',
				default: '',
				description: 'The user  to Disable in Keycloak',
			},
			{
				displayName: 'Realm',
				name: 'realm',
				type: 'string',
				default: '',
				description: 'The Keycloak realm under which the email check should be performed',
			},
			{
				displayName: 'Grant Type',
				name: 'grantType',
				type: 'options',
				options: [
					{
						name: 'Client Credentials',
						value: 'client_credentials',
					},
					// Add other grant types as needed
				],
				default: 'client_credentials',
				description: 'The OAuth 2.0 grant type for obtaining the token',
			},
			{
				displayName: 'Active Token',
				name: 'activeToken',
				type: 'string',

				default: '',
				description: 'Token for auth Keycloak',
			},
		],
	};

	async execute(this: IExecuteFunctions): Promise<any> {
		let outputData: any = [];

		//const items = this.getInputData();
		const realm: string = this.getNodeParameter('realm', 0) as string;
		const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
		const grantType = this.getNodeParameter('grantType', 0) as string;

		const credentials = (await this.getCredentials('keycloakCredentialsApi')) as {
			serverUrl: string;
			apiToken: string;
			clientId?: string;
			clientSecret?: string;
			defaultRealm?: string;
		};
		let token: any = activeToken || '';
		if (!token && credentials.apiToken) token = credentials.apiToken;

		const userEndpoint = `${credentials.serverUrl}realms/${
			realm || credentials.defaultRealm
		}/protocol/openid-connect/token`;
		if (!token && credentials.clientId && credentials.clientSecret) {
			//get token
			const requestObj = {
				method: 'POST',
				uri: userEndpoint,
				body: `grant_type=${grantType}&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
				json: true,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					Authorization:
						'Basic ' +
						Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64'),
				},
			};

			const responseData = await this.helpers.request(requestObj);

			token = responseData.access_token;
		}
		if (!token) {
			outputData.push({
				json: { result: false, message: 'Failed to obtain Keycloak token.' },
			});
			return outputData;
		}

		try {
			//for (let i = 0; i < items.length; i++) {
			//const item = items[0];

			const user = this.getNodeParameter('username', 0) as string;
			const encodedUserName = encodeURIComponent(`${user}`);
			const apiUrl = `${credentials.serverUrl}admin/realms/${
				realm || credentials.defaultRealm
			}/users`;
			const headers = {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			};

			// Step 1: Get the User ID
			const searchUrl = `${apiUrl}?username=${encodedUserName}`;

			const searchOptions = {
				method: 'GET',
				uri: searchUrl,
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json',
				},
				json: true,
			};

			const searchResponse = await this.helpers.request(searchOptions);
			//console.log('User Object 🚀🚀🚀 ~ line 140', searchResponse);
			if (searchResponse != undefined) {
				const user = searchResponse[0]; // Assuming the username is unique and we get one result
				if (user) {
					const userId = user.id;

					// Step 2: Disable the User
					const updateUrl = `${apiUrl}/${userId}`;
					const updateUser = {
						enabled: false,
					};

					//console.log('User Object 🚀🚀🚀 ~ line 152', user);

					const updateOptions = {
						method: 'PUT',
						uri: updateUrl,
						headers: headers,
						body: updateUser, // Directly pass the object here
						json: true, // This tells the library to send the body as JSON
					};

					const responseData = await this.helpers.request(updateOptions);
					//console.log('Response Data', responseData);
					if (responseData == undefined) {
						return [[{ json: { result: 200, message: 'User Disabled' } }]];
					} else {
						return [[{ json: { result: 400, message: 'Failed to disable user' } }]];
					}
				}
			} else {
				//console.error('Failed to find for user:', searchResponse);
				return [[{ json: { result: 400, message: 'Failed to find user' } }]];
			}
		} catch (error) {
			return [
				[
					{
						json: {
							result: 400,
							message: error.message,
							errorstack: error.stack,
						},
					},
				],
			];
		}
	}
}
