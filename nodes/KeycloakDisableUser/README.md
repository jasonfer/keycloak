# Keycloak Disable User Node

This node provides functionality to disable a user within a specified realm in Keycloak.

![Keycloak Disable User Icon](KeycloakDisableUser.node.svg)

## Description

The `Keycloak Disable User` node allows you to programmatically disable an existing user in a Keycloak realm. It can locate a user by their username or user ID and change their status to disabled.

## Configuration

### Inputs

- **Main Input**: This is where you provide the data for the user to be disabled.

### Outputs

- **success**: Indicates that the user was successfully disabled in the specified Keycloak realm.
- **error**: Indicates an error in the user disabling process, such as authentication issues, server errors, or if the user was not found.

### Credentials

- **keycloakCredentialsApi**: These credentials are required for this node. They should include the Server URL, optionally an API Token, Client ID, Client Secret, and a default realm.

### Parameters

- **User Identifier**: Choose between 'Username' or 'User ID' to specify how to identify the user to be disabled.

- **Username/User ID**: The username or user ID of the user to be disabled, depending on the chosen User Identifier.

- **Realm**: The Keycloak realm in which the user should be disabled. If this is left blank, the default realm provided in the credentials will be used.

- **Grant Type**: The OAuth 2.0 grant type used for obtaining the token. Currently, the only available option is `Client Credentials`, but more can be added if necessary.

- **Active Token**: A token for authenticating with Keycloak. If this is left blank and if the API Token is also not provided in the credentials, the node will try to obtain a token using the Client ID and Client Secret.

## Usage

1. Connect your data source containing user information to the main input.
2. Choose whether to identify the user by Username or User ID.
3. Provide the username or user ID of the user to be disabled.
4. Specify the realm if different from the default.
5. Provide the necessary Keycloak credentials or an Active Token.
6. Execute the node. You will receive either a `success` or `error` output depending on the result of the user disabling process.

## Example

Assuming you want to disable a user with the username "john.doe" in the Keycloak realm "myRealm":

1. Connect your data source to the main input.
2. Set the `User Identifier` parameter to "Username".
3. Set the `Username/User ID` parameter to "john.doe".
4. Set the `Realm` parameter to "myRealm".
5. Choose `Client Credentials` as the Grant Type.
6. Provide the necessary Keycloak credentials or an Active Token.
7. Execute the node. If the user is disabled successfully, you will get a `success` output. In case of any errors or if the user is not found, the `error` output will be triggered.

Always ensure sensitive data, like tokens, credentials, and user information, are handled securely.
