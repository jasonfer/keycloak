import {
    INodeType,
    INodeTypeDescription,
    IExecuteFunctions,
} from 'n8n-workflow';


/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakGetToken implements INodeType {
    description: INodeTypeDescription = {
        displayName: 'get token Keycloak',
        name: 'keycloakGetToken',
        icon: 'file:KeycloakGetToken.node.svg', // Provide a path to your icon
        group: ['input'],
        version: 2,
        description: 'get a fresh token for credential Keycloak',
        defaults: {
            name: 'get token Keycloak',

        },
        inputs: ['main'],
        outputs: ['main'],
        credentials: [
            {
                name: 'keycloakCredentialsApi',
                required: true,
            },
        ],
        properties: [
            {
                displayName: 'Realm',
                name: 'realm',
                type: 'string',
                default: '',
                description: 'The Keycloak realm under which the email check should be performed',
            },
            {
                displayName: 'Grant Type',
                name: 'grantType',
                type: 'options',
                options: [
                    {
                        name: 'Client Credentials',
                        value: 'client_credentials',
                    },
                    {
                        name: 'Password',
                        value: 'password',
                    },
                    // Add other grant types as needed
                ],
                default: 'client_credentials',
                description: 'The OAuth 2.0 grant type for obtaining the token',
            },
            {
                displayName: 'Username',
                name: 'username',
                type: 'string',
                placeholder: 'name@email.com',
                default: '',
                displayOptions: {
                    show: {
                        grantType: [
                            'password'
                        ]
                    }
                },
                description: 'The Keycloak username',
            },
            {
                displayName: 'Email',
                name: 'email',
                type: 'string',
                placeholder: 'name@email.com',
                default: '',
                displayOptions: {
                    show: {
                        grantType: [
                            'password'
                        ]
                    }
                },
                description: 'The Keycloak email',
            },
            {
                displayName: 'Password',
                name: 'password',
                type: 'string',
                typeOptions: {
                    password: true
                },
                default: '',
                displayOptions: {

                    show: {

                        grantType: [
                            'password'
                        ]
                    }
                },
                description: 'The Keycloak email',
            },
            {
                displayName: 'Audiance',
                name: 'audiance',
                type: 'string',
                default: 'account',
                displayOptions: {
                    show: {
                        grantType: [
                            'password'
                        ]
                    }
                },
                description: 'The Keycloak audiance',
            }
        ],
    };

    async execute(this: IExecuteFunctions): Promise<any> {


        const realm: string = this.getNodeParameter('realm', 0) as string;

        const grantType = this.getNodeParameter('grantType', 0) as string;

        const credentials = await this.getCredentials('keycloakCredentialsApi') as {
            serverUrl: string;
            apiToken: string;
            clientId?: string;
            clientSecret?: string;
            defaultRealm?: string;
        };
        let token: any =  "";
        if(!token && credentials.apiToken)
            token = credentials.apiToken;

        const userEndpoint = `${credentials.serverUrl}realms/${encodeURIComponent(realm || credentials.defaultRealm || '')}/protocol/openid-connect/token`;

        if (token || (credentials.clientId && credentials.clientSecret)) {
            let body= `grant_type=${grantType}&client_id=${encodeURIComponent(credentials.clientId||'')}&client_secret=${encodeURIComponent(credentials.clientSecret||'')}`
            if(grantType=='password') {
                const email  = this.getNodeParameter('email', 0) as string;
                const password  = this.getNodeParameter('password', 0) as string;
                const audiance  = this.getNodeParameter('audiance', 0) as string;
                const username = this.getNodeParameter('username', 0)  as string;
                body = `grant_type=${encodeURIComponent(grantType)}&audiance=${encodeURIComponent(audiance)}&username=${encodeURIComponent(username)}&email=${encodeURIComponent(email)}&password=${encodeURIComponent(password)}`
            }
            const requestObj = {
                method: 'POST',
                uri: userEndpoint,
                body: body,
                json: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization':
                        'Basic ' +
                        Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64')
                },
            }

            const responseData = await this.helpers.request(requestObj);

            return [[{
                json: {token: responseData.access_token},
            }]];
        }
        if (!token) {

            return [[{
                json: {result: false, message: 'Failed to obtain Keycloak token.'},
            }]];
        }
    }
}
