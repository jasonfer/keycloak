# Keycloak Get Token Node for n8n

This is a custom node for [n8n](https://n8n.io/) that allows you to obtain a fresh token from Keycloak for credential authentication.

## Prerequisites

Before using this node, make sure you have the following:

- [n8n](https://n8n.io/) installed and configured.
- Access to a Keycloak server with the necessary credentials.

## Installation

1. Open your n8n workflow.
2. Click on "Nodes" in the top menu.
3. Click on the "Manage Palette" option.
4. In the "Search" tab, search for "Keycloak Get Token" and click "Install."

## Usage

1. Add the "Keycloak Get Token" node to your workflow.
2. Configure the node by providing the required credentials and options.
3. Connect the "Keycloak Get Token" node to other nodes in your workflow to use the obtained token for authentication.

## Node Configuration

- **Realm**: The Keycloak realm under which the token should be obtained.
- **Grant Type**: The OAuth 2.0 grant type for obtaining the token. (e.g., "Client Credentials")

## Credentials

- **keycloakCredentialsApi**: Provide the necessary Keycloak server URL, API token, and other authentication details.

## Example

Here's an example of how to use the "Keycloak Get Token" node in an n8n workflow:

1. Start with a Trigger node (e.g., HTTP Request).
2. Add the "Keycloak Get Token" node to obtain a fresh token.
3. Use the obtained token in subsequent nodes for authentication with Keycloak.

## License

This custom n8n node is released under the [MIT License](LICENSE).

## Author

This node was created by [Your Name].

For more information, visit the [n8n community](https://community.n8n.io/).

