# Keycloak Token Verify Node for n8n

![Keycloak Token Verify Node Icon](./KeycloakTokenVerify.node.svg)

The Keycloak Token Verify node for `n8n` allows you to verify the validity of a token against your Keycloak instance.

## Features

- Verify the validity of a token in Keycloak.
- Three possible outputs: `ok`, `ko`, and `error`, allowing you to handle different scenarios in your workflow.
- Configurable through the n8n UI with custom parameters or credentials.

## Configuration Options

### Inputs
- **Main Input**: This is where you provide the data that contains the token to be verified.
- **Token Key**: The key where the token can be found. By default, it expects the token to be in `headers.authorization`. Note that it will remove the "Bearer" keyword automatically.
- **Realm**: The Keycloak realm under which the token verification should be performed. If left empty, the realm specified in the node's credentials will be used.

### Credentials

- **Keycloak Credentials**: This is where you specify the connection details to your Keycloak instance. The required details include:
    - **Server URL**: The URL of your Keycloak server.
    - **Client ID**: Your Keycloak client ID.
    - **Client Secret**: Your Keycloak client secret.
    - **Default Realm**: If the realm isn't specified in the input, this default realm will be used.

### Outputs

- **ok**: Returns true if the token is valid.
- **ko**: Returns false if the token is invalid.
- **error**: Returns an error message if there was an issue during the verification process.

## How to Use

1. Drag and drop the `Keycloak Token Verify` node into your n8n canvas.
2. Connect it


### Example

If you have data with a header containing an authorization token (`Bearer xxxxx...`), and you want to verify its validity:

1. Connect your data source to the main input.
2. Set the `Token Key` parameter to `headers.authorization`.
3. Provide the necessary Keycloak credentials.
4. Execute the node. Depending on the token's validity, the data will be routed to either the `ok` or `ko` outputs.