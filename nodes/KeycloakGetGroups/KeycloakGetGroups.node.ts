import { INodeType, INodeTypeDescription, IExecuteFunctions } from 'n8n-workflow';

// const ok = 0,
// 	error = 2;

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakGetGroups implements INodeType {
	description: INodeTypeDescription = {
		displayName: 'Get Keycloak Groups',
		name: 'keycloakGetGroups',
		icon: 'file:keycloakGetGroup.node.svg', // Provide a path to your icon
		group: ['input'],
		version: 2,
		description: 'Get Keycloak Groups',
		defaults: {
			name: 'Get Keycloak Groups',
		},
		inputs: ['main'],
		// eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
		outputs: ['main'],
		credentials: [
			{
				name: 'keycloakCredentialsApi',
				required: true,
			},
		],
		properties: [
			{
				displayName: 'Realm',
				name: 'realm',
				type: 'string',
				default: '',
				description: 'The Keycloak realm under which the email check should be performed',
			},
			{
				displayName: 'Grant Type',
				name: 'grantType',
				type: 'options',
				options: [
					{
						name: 'Client Credentials',
						value: 'client_credentials',
					},
					// Add other grant types as needed
				],
				default: 'client_credentials',
				description: 'The OAuth 2.0 grant type for obtaining the token',
			},
			{
				displayName: 'Active Token',
				name: 'activeToken',
				type: 'string',

				default: '',
				description: 'Token for auth Keycloak',
			},
		],
	};

	async execute(this: IExecuteFunctions): Promise<any> {
		let outputData: any = [];

		//const items = this.getInputData();
		const realm: string = this.getNodeParameter('realm', 0) as string;
		const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
		const grantType = this.getNodeParameter('grantType', 0) as string;

		const credentials = (await this.getCredentials('keycloakCredentialsApi')) as {
			serverUrl: string;
			apiToken: string;
			clientId?: string;
			clientSecret?: string;
			defaultRealm?: string;
		};
		let token: any = activeToken || '';
		if (!token && credentials.apiToken) token = credentials.apiToken;

		const userEndpoint = `${credentials.serverUrl}realms/${
			realm || credentials.defaultRealm
		}/protocol/openid-connect/token`;
		if (!token && credentials.clientId && credentials.clientSecret) {
			//get token
			const requestObj = {
				method: 'POST',
				uri: userEndpoint,
				body: `grant_type=${grantType}&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
				json: true,
				headers: {
					Authorization: `Bearer ${token}`,
				},
			};

			const responseData = await this.helpers.request(requestObj);

			token = responseData.access_token;
		}
		if (!token) {
			outputData.push({
				json: { result: false, message: 'Failed to obtain Keycloak token.' },
			});
			return outputData;
		}

		try {
			const apiUrl = `${credentials.serverUrl}admin/realms/${
				realm || credentials.defaultRealm
			}/groups`;
			const headers = {
				Authorization: `Bearer ${token}`,
			};
			const requestObj = {
				method: 'GET',
				uri: apiUrl,
				headers: headers,
			};

			const responseData = await this.helpers.request(requestObj);

			const data = JSON.parse(responseData);

			return [this.helpers.returnJsonArray(data)];
		} catch (error) {
			return [
				[
					{
						json: {
							result: false,
							message: error.message,
							item: outputData,
						},
					},
				],
			];
		}
	}
}
