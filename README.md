![Banner image](https://user-images.githubusercontent.com/10284570/173569848-c624317f-42b1-45a6-ab09-f0ea3c247648.png)
# Keycloak Nodes for n8n

This npm module provides a set of nodes to interact with Keycloak, offering functionalities ranging from email verification to token validation and sending emails.

## Nodes Included

1. [**Keycloak Token Verify**](#keycloak-token-verify): Verifies the validity of a token in Keycloak.
2. [**Keycloak Send Email**](#keycloak-send-email): Sends a reset password email or other action-based emails in Keycloak.
3. [**Keycloak Email Verify**](#keycloak-email-verify): Checks if an email exists within a specified realm in Keycloak.
4. [**Keycloak Get Token**](#keycloak-token-verify): Get a new token in Keycloak (login password).

---

### Keycloak Token Verify

Verifies the validity of a token in Keycloak.

[Detailed Description & Usage](nodes%2FKeycloakTokenVerify%2FREADME.md)

---

### Keycloak Send Email

Sends a reset password email or other action-based emails in Keycloak.

[Detailed Description & Usage](nodes%2FKeycloakSendEmail%2FREADME.md)

---

### Keycloak Email Verify

Checks if an email exists within a specified realm in Keycloak.

[Detailed Description & Usage](nodes%2FKeycloakEmailVerify%2FREADME.md)

---

### Keycloak  get token

get a token using 2 methods, login/password or credentials

[Detailed Description & Usage](nodes%2FKeycloakGetToken%2FREADME.md)

---

## Installation

Provide the installation instructions for your npm module here.

## License

Mention the license for your npm module here, e.g., MIT, GPL, etc.

---

For any issues, requests, or contributions, please refer to the repository's issue tracker.



## TODO
Refactoring for enhanced token management

User Management:

- Create User 
- Get User 
- Update User
- Delete User
- Role Mappings
- Add Role Mappings to User

## Resources

* [n8n community nodes documentation](https://docs.n8n.io/integrations/community-nodes/)
* _Link to app/service documentation._

## More information

Refer to our [documentation on creating nodes](https://docs.n8n.io/integrations/creating-nodes/) for detailed information on building your own nodes.

## License

[MIT](https://github.com/n8n-io/n8n-nodes-starter/blob/master/LICENSE.md)
