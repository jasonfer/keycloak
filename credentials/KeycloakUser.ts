export interface KeycloakUser {
	firstName: string;
	lastName: string;
	username: string;
	email: string;
	enabled: string;
	credentials?: Credential[];
	groups?: string[];
	attributes?: {
		[key: string]: string[];
	};
}

export interface Credential {
	type: string;
	value: string;
	temporary: string;
}
